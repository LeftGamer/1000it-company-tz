<?php

namespace App\Http\Resources;

use App\Http\Resources\BaseResource;
use App\Models\News;

/**
 * @property News $resource
 */

class NewsResource extends BaseResource
{
    public function toArray($request)
    {
        /** @var News $news */
        $news = $this->resource;
        return [
            'id' => $news->id,
            'title' => $news->title,
            'announcement' => $news->announcement,
            'text' => $news->text,
            'date_publicate' => $news->date_publicate,
            'author' => $news->author,
            'category'  => $news->category,
            'created_at' => $news->created_at,
        ];
    }
}
