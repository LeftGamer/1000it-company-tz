<?php

namespace App\Http\Resources;

use App\Models\Author;
use Illuminate\Http\Resources\Json\JsonResource;

class AuthorsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Author $author */
        $author = $this->resource;
        return [
            'id' => $author->id,
            'fio' => $author->fio,
            'email' => $author->email,
            'avatar' => $author->avatar,
            'created_at' => $author->created_at
        ];
    }
}
