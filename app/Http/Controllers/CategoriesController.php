<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoriesRequest;
use App\Models\Category;

class CategoriesController extends Controller
{
    public function create()
    {
        return view(
            'categories.create'
        );
    }

    public function store(CategoriesRequest $request)
    {
        $data = $request->all();

        $model = new Category();

        $model->fill(
            [
                'society'  => $data[ 'society' ],
                'city_day' => $data[ 'city_day' ],
                'sport'    => $data[ 'sport' ],

            ]
        );
        $model->save();
    }
}
