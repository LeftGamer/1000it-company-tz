<?php

namespace App\Http\Controllers\Api;

use App\Models\News;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    static public function getAuthor(Request $request)
    {
        $author = $request->get('author');

        $newsModel = News
            ::query()
            ->with('author')
            ->where(
                function ($q) use ($author)
                {
                    if ($author) {
                        $q->where('author', $author);
                    }
                }
            )
            ->get()
        ;

        return $newsModel
            ->map(
                function ($item)
                {
                    return [
                        'title'          => $item->title,
                        'preview'        => $item->announcement,
                        'text'           => $item->text,
                        'date_publicate' => $item->date_publicate,
                        'category'       => $item->category,
                    ];
                }
            );
    }

    static public function getCategory(Request $request)
    {
        $category = $request->get('category');

        $categoryData = News
            ::query()
            ->with('category')
            ->where(function ($query) use ($category)
            {
                if($category)
                {
                    $query->where('category', $category);
                }
            }
            )
            ->get();

            return $categoryData
                ->map(function ($item)
                {
                    return [
                        'title'          => $item->title,
                        'preview'        => $item->announcement,
                        'text'           => $item->text,
                        'date_publicate' => $item->date_publicate,
                        'author'       => $item->author,
                    ];
                });
    }

    static public function getAll()
    {
        $model = News
            ::query()
            ->with(['category', 'author'])
            ->whereHas('author')
            ->whereHas('category')
            ->get();

        return $model
            ->map(function ($item)
            {
                return [
                    'title'          => $item->title,
                    'preview'        => $item->announcement,
                    'text'           => $item->text,
                    'date_publicate' => $item->date_publicate,
                    'author'       => $item->author,
                    'category'       => $item->category,
                ];
            });
    }

    static public function searchAndFilter()
    {
        if (request()->ajax()) {
            $q = News::open()
                     ->with([ 'category', 'author' ])
                     ->orderBy('created_at', 'desc')
            ;
            if (request('searchLine')) {
               $q->where('text', 'like', '%'.request('searchLine').'%');
                $q->where('category', 'like', '%'.request('searchLine').'%');
            }
                $q->get();

            return response()->json(['success' => true]);
        }
    }
}
