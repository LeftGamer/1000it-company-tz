<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewsRequest;
use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function create()
    {
        return view(
            'news.create'
        );
    }

    public function store(NewsRequest $request, int $categoryId, int $authorId)
    {
        $data = $request->all();

        $newsModel = new News();

        $newsModel->fill(
            [
                'title'          => $data[ 'title' ],
                'announcement'   => $data[ 'preview' ],
                'text'           => $data[ 'text' ],
                'date_publicate' => $data[ 'date_publicate' ],
                'category'       => $data[ 'category' ],
                'author'         => $data[ 'author' ],
            ]
        );
        $newsModel->save();
    }
}
