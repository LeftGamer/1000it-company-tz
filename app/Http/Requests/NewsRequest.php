<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        $rules = [
            'title' => 'required|string',
            'preview' => 'required|string',
            'text' => 'required|string',
            'date_publicate' => 'required|date',
            'category' => 'required|string',
            'author' => 'required|string',
        ];
        return $rules;
    }

    public function attributes()
    {
        return [
            'title'  => trans('Заголовок'),
            'preview'  => trans('Анонс'),
            'text'  => trans('Текст'),
            'date_publicate'  => trans('Дата и время публикаций'),
            'category'  => trans('Рубрика'),
            'author'  => trans('Автор'),
        ];
    }
}
