<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/news/{author}', [\App\Http\Controllers\Api\NewsController::class, 'getAuthor'])->name('news.author');
Route::get('/author', [\App\Http\Controllers\Api\AuthorsController::class, 'index'])->name('author');
Route::get('/news{category}', [\App\Http\Controllers\Api\NewsController::class, 'getCategory'])->name('news.category');
Route::get('/news{all}', [\App\Http\Controllers\Api\NewsController::class, 'getAll'])->name('news.all');
Route::get('/news/search', [\App\Http\Controllers\Api\NewsController::class, 'searchAndFilter'])->name('news.filter');
